
public class encrypt
{
  
 public static void main(String[] args){
    
     System.out.println ( normalizeText("This is  \"really\" great. (Text)!?"));
     System.out.println ( caesarify("This is  \"really\" great. (Text)!?", 3));
    }
 
 public static String normalizeText( String t){
    t = t.replace ( " ", "");
    t = t.replace ( ",", "");
    t = t.replace ( ".", "");
    t = t.replace ( ":", "");
    t = t.replace ( "?", "");
    t = t.replace ( ";", "");
    t = t.replace ( "'", "");
    t = t.replace ( "\"", "");
    t = t.replace ( "!", "");
    t = t.replace ( "\\", "");
    t = t.replace ( "/", "");
    t = t.replace ( "(", "");
    t = t.replace ( ")", "");
    t = t.toUpperCase();
    return  (t);
}
public static String caesarify (String p , int v){
    String nw = shiftAlphabet(v);
    String result ="";
    for (int i = 0 ; i < p.length (); i++) {
        char c = p.charAt(i);
        result += p.substring(i, i+1).replace(c, nw.charAt((int)c -(int) 'A'));
    }
    return result;
   
}



public static String shiftAlphabet(int shift) {
    int start = 0;
    if (shift < 0) {
        start = (int) 'Z' + shift + 1;
    } else {
        start = 'A' + shift;
    }
    String result = "";
    char currChar = (char) start;
    for(; currChar <= 'Z'; ++currChar) {
        result = result + currChar;
    }
    if(result.length() < 26) {
        for(currChar = 'A'; result.length() < 26; ++currChar) {
            result = result + currChar;
        }
    }
    return result;
}
}

