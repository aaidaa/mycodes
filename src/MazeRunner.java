import java.util.*;
public class MazeRunner {
    public static void main (String args[]) {
        Maze myMap = new Maze();
        intro();
        myMap.printMap();
        userMove();

        if (myMap.canIMoveRight() & myMap.canIMoveLeft()& myMap.canIMoveUp()& myMap.canIMoveDown()) {
            System.out.println("good to go");
        }
            else{

            System.out.println("Sorry, you’ve hit a wall.");
            }
        }



    //Welcome the user to Maze Runner and show them the current state of the Maze
    public static void intro(){
        System.out.println("Welcome to Maze Runner!");
        System.out.println("HEre is your current position:");

    }

    //offer your user a way to enter in which direction they would like to move, Right (R), Left (L), Up (U) or Down (D)
    public static String userMove (){
        System.out.println("Where would you like to move? (R, L, U, D)");
        Scanner input = new Scanner(System.in);
        String d = input.nextLine();
        if (d.equals("R") || d.equals("L") || d.equals("U") || d.equals("D") ){
            return d;
        }
            else {
            userMove();
            return "Invalid Entry";
        }
    }



}
